var express = require('express')
var body_parser = require('body-parser')
var fs = require('fs')
var mongodb = require('mongodb')

var configs = require('./data/configs')

var app = express()
// var port = 3030
const port = process.ENV.PORT
var mongo_client = mongodb.MongoClient
var connection_string = 'mongodb://osethan:H7v8nyAWWSNDh0YN@ds231723.mlab.com:31723/tsn'

mongo_client.connect(connection_string, {
  useNewUrlParser: true
}, function (err, client) {
  if (err) {
    console.log('ERROR')
  }

  var db = client.db('tsn')
  var solutions_coll = db.collection('solutions')

  app.use(body_parser.json())
  app.use(function (req, res, next) {
    // var client_url = 'http://localhost:3000'
    const client_url = 'https://triangle-solo-noble.herokuapp.com/'

    res.header('Access-Control-Allow-Origin', client_url)
    res.header('Access-Control-Allow-Headers', 'Content-Type, Accept')
    next()
  })

  app.get('/history', function (req, res) {
    fs.readFile('./data/history.json', 'utf8', function (err, content) {
      if (err) {
        console.log('ERROR')
      }

      var history = JSON.parse(content)

      res.json(history)
    })
  })

  app.post('/solution', function (req, res) {
    var index = Number(req.body.index)
    var query = {
      index: index
    }

    solutions_coll.findOne(query, function (err, doc) {
      if (err) {
        console.log('ERROR')
      }

      var solution = doc.solution

      res.json(solution)
    })
  })

  app.get('/rules', function (req, res) {
    fs.readFile('./data/rules.json', 'utf8', function (err, content) {
      if (err) {
        console.log('ERROR')
      }

      var rules = JSON.parse(content)

      res.json(rules)
    })
  })

  app.listen(port)
})
